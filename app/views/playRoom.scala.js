@* jsファイル内で使用する引数 *@
@(roomId: String, userId: String)

$(function() {
	@* エラー領域非表示 *@
	$("#onError").hide();

	@* WebSocketオブジェクトの取得 *@
	var WS = window['MozWebSocket'] ? MozWebSocket : WebSocket
	var chatSocket = new WS("@routes.PlayRoomController.initPlay(roomId,userId).webSocketURL(request)")

	@* クリックしたカード文字 *@
	var cardObj;
	@* ユーザIDの保持 *@
	var id = "";
	
	@* WebSocketへのメッセージ送信(WebSocketの.sendメソッド) *@
	var sendMessage = function() {
		chatSocket.send(JSON.stringify(
			{text: $("#talk").val()}
		))
		$("#talk").val('')
	};

	@* カードをクリックした時のイベント *@
	$("#card div").click(function(){
		$("#card").find("div").css("background-color","#FFF");
		$(this).css("background-color","#746");
		var input = prompt("「" + $("#ba").text() + "」" + "で始まり" + "「" + $(this).text() + "」" + "で終わる言葉を入力", "");
		var flg;

		if(input === null){
			return;
		}else{
			if(input.substring(0,1) != $("#ba").text() || input.substring(input.length - 1,input.length) != $(this).text()){
				alert("やり直し1！！！");
				return;
			}else{
				//for(var i = 0; i<=dictionary.length; i++){
				//alert("in");
				//if(input == dictionary[i])
				if($.inArray(input,dictionary)!=-1){
					//for(var j = 0; j < guruguru.length; j++){
					//if($.inArray($(input.charAt(-1)),guruguru)!=-1)
					//{
					flg = '1'
					//break;
					//}
					//}
				}else{
					flg = '0'
				} 
				//}
				if(flg=='0'){
					alert("やり直し2！！！");
					return;
				}else if(flg=='1'){
					alert("正解");
				}
			}
		}
		cardObj = this;
		chatSocket.send(JSON.stringify( {text: input}));
	});

	@* WebSocketでメッセージを受けとった際に行う処理 *@
	var receiveEvent = function(event) {
		var data = JSON.parse(event.data)
		// Handle errors
		if(data.error) {
			chatSocket.close();
			$("#onError span").text(data.error);
			$("#onError").show();
			return;
		}else if("join" == data.kind) {
			receiveJoin(data);
		}else if("talk" == data.kind) {
			alert("receiveTalk");
			receiveTalk(data);
		}else if("quit" == data.kind) {
			receiveQuit(data);
		}
	};

	@* 入室時の処理 *@
	var receiveJoin = function(data){
		if(id == ""){
			id = data.user;
			alert("ゲーム開始");
		}else{
		}
	};

	@* ゲーム中処理 *@
	var receiveTalk = function(data){
		if(id == data.user){
			$("ul").append($("<li/>").text(data.message));
			$("#ba").text(data.ba);
			$(cardObj).remove();
			if($("#card div").length == 0){
				alert("Complete!");
			}
		}else{
			$("#ba").text(data.ba);
		}
	};
	
	@* 退室時の処理 *@
	var receiveQuit = function(data){
		
	};

	@* WebSocketからの送信データ受信(WebSocketの.onmessageメソッド) *@
	@* 送信データ受信時、receiveEventメソッドが呼ばれる *@
	chatSocket.onmessage = receiveEvent;

	@* テキストエリアでのEnterキーの挙動(WebSocketへのメッセージ送信) *@
	var handleReturnKey = function(e) {
		if(e.charCode == 13 || e.keyCode == 13) {
			e.preventDefault()
			sendMessage()
		}
	};

	$("#talk").keypress(handleReturnKey);

})
