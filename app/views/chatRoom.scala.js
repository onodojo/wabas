@* @(username: String)*@
@(roomname: String, username: String)

$(function() {
	@* WebSocketオブジェクトの取得 *@
	var WS = window['MozWebSocket'] ? MozWebSocket : WebSocket
	var chatSocket = new WS("@routes.Application.chat(roomname, username).webSocketURL(request)")
	@* var chatSocket = new WS("@routes.Application.chat(username).webSocketURL(request)") *@

	@* WebSocketへのメッセージ送信(WebSocketの.sendメソッド) *@
	var sendMessage = function() {
		chatSocket.send(JSON.stringify(
			{text: $("#talk").val()}
		))
		$("#talk").val('')
	}

	@* WebSocketでメッセージを受けとった際に行う処理 *@
	var receiveEvent = function(event) {
		var data = JSON.parse(event.data)

		// Handle errors
		if(data.error) {
			chatSocket.close()
			$("#onError span").text(data.error)
			$("#onError").show()
			return
		} else {
			$("#onChat").show()
		}

		// Create the message element
		var el = $('<div class="message"><span></span><p></p></div>')
		$("span", el).text(data.user)
		$("p", el).text(data.message)
		$(el).addClass(data.kind)
		if(data.user == '@username') $(el).addClass('me')
		$('#messages').append(el)

		// Update the members list
		$("#members").html('')
		$(data.members).each(function() {
			var li = document.createElement('li');
			li.textContent = this;
			$("#members").append(li);
		})
	}

	@* テキストエリアでのEnterキーの挙動(WebSocketへのメッセージ送信) *@
	var handleReturnKey = function(e) {
		if(e.charCode == 13 || e.keyCode == 13) {
			e.preventDefault()
			sendMessage()
		}
	}

	$("#talk").keypress(handleReturnKey)

	@* WebSocketからの送信データ受信(WebSocketの.onmessageメソッド) *@
	@* 送信データ受信時、receiveEventメソッドが呼ばれる *@
	chatSocket.onmessage = receiveEvent

})
