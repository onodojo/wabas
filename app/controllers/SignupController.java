package controllers;

import models.User;
import models.services.UserService;
import play.Logger;
import play.data.Form;
import play.data.validation.Constraints.MaxLength;
import play.data.validation.Constraints.Required;
import play.libs.F.Option;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.home;
import views.html.signup;

/**
 * サインアップ画面を管理するコントローラー
 * 
 * @author n.ohama
 *
 */
public class SignupController extends Controller {

	public static class SignupForm {

		@Required(message = "nameが空欄です")
		@MaxLength(8)
		public String name;

		@Required(message = "passwordが空欄です")
		@MaxLength(16)
		public String password;
	}

	/**
	 * サインアップ画面を返却する。
	 * 
	 * @return サインアップ画面
	 */
	public static Result index() {
		return ok(signup.render());
	}

	/**
	 * サインアップを行い、ルーム一覧画面を返却する。
	 * 
	 * @return ルーム一覧画面
	 */
	public static Result regist() {

		Form<SignupForm> f = Form.form(SignupForm.class).bindFromRequest();

		// validation
		if (f.hasErrors()) {
			return badRequest(f.errorsAsJson());
		}

		SignupForm data = f.get();
		User user = new User();
		user.name = data.name;
		user.password = data.password;

		UserService userService = new UserService();
		Option<User> userInfo = userService.save(user);

		Logger.debug(userInfo.get().id.toString());
		Logger.debug(userInfo.get().name.toString());
		session("userId", userInfo.get().id.toString());
		session("userName", userInfo.get().name.toString());
		return ok(home.render());
	}
}
