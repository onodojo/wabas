package controllers;

import com.fasterxml.jackson.databind.JsonNode;

import models.PlayRoom;
import play.*;
import play.mvc.*;
import views.html.*;

/**
 * Play画面を管理するコントローラー
 * 
 * @author Shinsuke
 *
 */
public class PlayRoomController extends Controller {

	/**
	 * ルーム一覧画面を返却する。
	 * @return ルーム一覧画面
	 */
    public static Result index() {
    	return ok(home.render());
    }

    /**
     * Play画面の表示
     * 
     * @param roomId 入室する部屋のID
     * @param userId 入室するユーザのID(本来はセッション情報から取得することとなる)
     * @return　Play画面
     */
    public static Result playRoom(String roomId, String userId){
        if(userId == null || userId.trim().equals("")) {
            flash("error", "Please choose a valid username.");
            return redirect(routes.Application.index());
        }
		Logger.debug(roomId);
		Logger.debug(userId);
        return ok(playRoom.render(roomId,userId));
    }

    /**
     * WebSocket用jsファイルの読み込み(playRoom.scala.jsの読み込み)
     * 
     * @param roomId 入室する部屋のID
     * @param userId 入室するユーザのID(本来はセッション情報から取得することとなる)
     * @return　jsファイル
     */
    public static Result playRoomJs(String roomId, String userId){
        return ok(views.js.playRoom.render(roomId,userId));
    }

    /**
     * クライアントにWebSocketオブジェクトを返却する
     * 
     * @param roomId 入室する部屋のID
     * @param userId 入室するユーザのID(本来はセッション情報から取得することとなる)
     * @return　WebSocketオブジェクト
     */
    public static WebSocket<JsonNode> initPlay(final String roomId, final String userId) {
        return new WebSocket<JsonNode>() {
            
            // Called when the Websocket Handshake is done.
            public void onReady(WebSocket.In<JsonNode> in, WebSocket.Out<JsonNode> out){
                
                // Join the chat room.
                try { 
                	PlayRoom.join(roomId,userId, in, out);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };
    }

}
