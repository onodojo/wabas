package controllers;


import models.User;
import models.services.UserService;
import play.*;
import play.data.DynamicForm;
import play.libs.F.Option;
import play.mvc.*;
import views.html.*;

/**
 * ログイン画面を管理するコントローラー
 * 
 * @author n.ohama
 *
 */
public class LoginController extends Controller {
	/**
	 * ログイン画面を返却する。
	 * @return ログイン画面
	 */
    public static Result index() {
    	return ok(login.render());
    }
    
	/**
	 * 認証を行い、ルーム一覧画面を返却する。
	 * @return ルーム一覧画面
	 */
    public static Result auth() {
    	DynamicForm form =  new DynamicForm().bindFromRequest();
		String userId = form.get("userId");
		String password = form.get("password");
		
		UserService userService = new UserService();
		Option<User> userInfo = userService.findByAuth(userId,password);

		if(userInfo.isDefined()){
			session("userId",userInfo.get().id.toString());
			session("userName",userInfo.get().name.toString());
			return redirect(routes.PlayRoomController.index());
		}else{
			flash("error","入力された内容が正しくありません。");
			return ok(login.render());
		}
    }
}
