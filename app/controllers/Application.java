package controllers;

import models.ChatRoom;

import com.fasterxml.jackson.databind.JsonNode;

import play.*;
import play.mvc.*;
import views.html.*;

public class Application extends Controller {

    /**
     * デフォルト
     */
    public static Result index() {
        return ok(index.render("Your new application is ready."));
    }

    /**
     * WebSocketのテスト
     * チャットルームの表示(chatRoom.scala.htmlの表示)
     * 
     * @param roomname
     * @param username
     * @return
     */
    public static Result chatRoom(String roomname, String username){
        if(username == null || username.trim().equals("")) {
            flash("error", "Please choose a valid username.");
            return redirect(routes.Application.index());
        }
		Logger.debug(roomname);
		Logger.debug(username);
        return ok(chatRoom.render(roomname,username));
    }

    /**
     * WebSocketのテスト
     * チャットルームの表示(chatRoom.scala.jsの読み込み)
     * 
     * @param roomname
     * @param username
     * @return
     */
    public static Result chatRoomJs(String roomname, String username){
        return ok(views.js.chatRoom.render(roomname,username));
    }

    /**
     * WebSocketのテスト
     * チャットルームの表示(クライアントにWebSocketオブジェクトを返却する)
     * 
     * @param roomname
     * @param username
     * @return
     */
    public static WebSocket<JsonNode> chat(final String roomname, final String username) {
        return new WebSocket<JsonNode>() {
            
            // Called when the Websocket Handshake is done.
            public void onReady(WebSocket.In<JsonNode> in, WebSocket.Out<JsonNode> out){
                
                // Join the chat room.
                try { 
                    ChatRoom.join(roomname,username, in, out);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };
    }

}
