package models;

import java.util.Date;

import javax.annotation.Nonnull;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import play.db.ebean.Model;
/**
 * UserBean
 *
 * @author n.ohama
 *
 */
@Entity
@Table(name="t_game")
public class Game extends Model {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	public Long id;
	@Nonnull
	@Column(name="match_date")
	public Date matchDate;
	@Column(name="rank1_user")
	public int rank1User;
	@Column(name="rank2_user")
	public int rank2User;
	@Column(name="rank3_user")
	public int rank3User;
	@Column(name="rank4_user")
	public int rank4User;
}
