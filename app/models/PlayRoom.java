package models;

import static akka.pattern.Patterns.ask;
import static java.util.concurrent.TimeUnit.SECONDS;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import play.Logger;
import play.libs.Akka;
import play.libs.Json;
import play.libs.F.Callback;
import play.libs.F.Callback0;
import play.mvc.WebSocket;
import scala.concurrent.Await;
import scala.concurrent.duration.Duration;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;

/**
 * Play画面のWebSocketを管理するコントローラー
 * 
 * @author Shinsuke
 *
 */
public class PlayRoom extends UntypedActor {

	/** ActorRefオブジェクト */
	static ActorRef currentRoom;

	/** ルームマップ */
	static Map<String,ActorRef> roomMap ;

	/** ルームごとにメンバーを保持するマップ */
	static Map<String,Map<String, WebSocket.Out<JsonNode>>> roomMenberMap ;

    //JoinなどでルームIDを引数として持たせれば、membersをルームごとに分けれるのでは？(メンバー追加用のメソッドあったほうがよい？)
    // Members of this room.
    Map<String, WebSocket.Out<JsonNode>> members = new HashMap<String, WebSocket.Out<JsonNode>>();

    // イニシャライザ
	static {
		roomMap = new HashMap<String,ActorRef>();
		roomMap.put("room1", Akka.system().actorOf(Props.create(PlayRoom.class)));
		roomMap.put("room2", Akka.system().actorOf(Props.create(PlayRoom.class)));
		
		roomMenberMap = new HashMap<String,Map<String, WebSocket.Out<JsonNode>>>();
		roomMenberMap.put("room1", new HashMap<String, WebSocket.Out<JsonNode>>());
		roomMenberMap.put("room2", new HashMap<String, WebSocket.Out<JsonNode>>());
	}

    /**
     * Join the default room.
     */
    public static void join(final String roomId, final String userId, WebSocket.In<JsonNode> in, WebSocket.Out<JsonNode> out) throws Exception{
        // RoomごとのActorRefオブジェクトの取得
    	currentRoom = roomMap.get(roomId);
        
        // Send the Join message to the room
        String result = (String)Await.result(ask(currentRoom,new Join(roomId, userId, out), 1000), Duration.create(1, SECONDS));
        
        if("OK".equals(result)) {
            
            // For each event received on the socket,
            in.onMessage(new Callback<JsonNode>() {
               public void invoke(JsonNode event) {
                   
                   // Send a Talk message to the room.
            	   currentRoom.tell(new Talk(roomId, userId, event.get("text").asText()), null);
                   
               } 
            });
            
            // When the socket is closed.
            in.onClose(new Callback0() {
               public void invoke() {
                   
                   // Send a Quit message to the room.
            	   currentRoom.tell(new Quit(roomId, userId), null);
                   
               }
            });
            
        } else {
            
            // Cannot connect, create a Json error.
            ObjectNode error = Json.newObject();
            error.put("error", result);
            
            // Send the error to the socket.
            out.write(error);
            
        }
        
    }

	@Override
	public void onReceive(Object message) throws Exception {
		//roomのメンバー取得
		Map<String, WebSocket.Out<JsonNode>> members;
		
		
        if(message instanceof Join) {
            
            // Received a Join message
            Join join = (Join)message;
            members = roomMenberMap.get(join.roomId);
            
            // Check if this username is free.
            if(members.containsKey(join.userId)) {
                getSender().tell("This username is already used", getSelf());
            } else {
                members.put(join.userId, join.channel);
//                notifyAll("join", join.username, "has entered the room");
                notifyAll(members,"join", join.userId, "START");
                getSender().tell("OK", getSelf());
            }
            
        } else if(message instanceof Talk)  {
            
            // Received a Talk message
            Talk talk = (Talk)message;
            members = roomMenberMap.get(talk.roomId);
            
    		Logger.debug(talk.userId);
    		Logger.debug(talk.text);
            notifyAll(members,"talk", talk.userId, talk.text);
            
        } else if(message instanceof Quit)  {
            
            // Received a Quit message
            Quit quit = (Quit)message;
            members = roomMenberMap.get(quit.roomId);
            
            members.remove(quit.userId);
            
            notifyAll(members,"quit", quit.userId, "has left the room");
        
        } else {
            unhandled(message);
        }
        
	}

	
    // 全メンバーへのメッセージ送信
    public void notifyAll(Map<String, WebSocket.Out<JsonNode>> members, String kind, String user, String text) {
        for(WebSocket.Out<JsonNode> channel: members.values()) {
            
            ObjectNode event = Json.newObject();
            event.put("kind", kind);
            event.put("user", user);
            event.put("message", text);
            event.put("ba", text.substring(text.length() - 1,text.length()));
            
            ArrayNode m = event.putArray("members");
            for(String u: members.keySet()) {
                m.add(u);
            }
            
            channel.write(event);
        }
    }



	// -- Messages
	public static class Join {
		final String roomId;
		final String userId;
		final WebSocket.Out<JsonNode> channel;
		
		public Join(String roomId, String userId, WebSocket.Out<JsonNode> channel) {
			this.roomId = roomId;
			this.userId = userId;
			this.channel = channel;
		}
	}
	
	public static class Talk {
		final String roomId;
		final String userId;
		final String text;
		
		public Talk(String roomId, String userId, String text) {
			this.roomId = roomId;
			this.userId = userId;
			this.text = text;
		}
	}
	
	public static class Quit {
		final String roomId;
		final String userId;
		
		public Quit(String roomId, String userId) {
			this.roomId = roomId;
			this.userId = userId;
		}
	}

}
