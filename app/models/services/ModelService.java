package models.services;

import java.util.List;

import play.db.ebean.Model;
import play.libs.F.Option;

public interface ModelService<T extends Model> {
	public Option<T> findById(Integer id);
	public Option<T> save(T entry);
	public List<Option<T>> findAll();
}
