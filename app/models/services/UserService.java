package models.services;

import java.util.ArrayList;
import java.util.List;

import models.User;
import play.libs.F;
import play.libs.F.Option;

import com.avaje.ebean.Query;

/**
 * UserService
 * 
 * @author n.ohama
 *
 */
public class UserService implements ModelService<User> {

	/**
	 * findAll
	 * 
	 */
	@Override
	public List<Option<User>> findAll() {
		// get UserList
		List<User> userList = User.find.all();
		List<Option<User>> optionList = new ArrayList<Option<User>>();

		// get User
		for (User user : userList) {
			Option<User> optionUser = apply(user);
			optionList.add(optionUser);
		}

		return optionList;
	}

	/**
	 * findByAuth
	 * 
	 * @param email
	 * @param password
	 * 
	 */
	public Option<User> findByAuth(String userId, String password) {
		// check email or password
		if (isEmpty(userId) || isEmpty(password)) {
			return null;
		}
		// create query
		StringBuilder sb = new StringBuilder("login_id='");
		sb.append(userId);
		sb.append("' And password='");
		sb.append(password);
		sb.append("'");
		Query<User> query = User.find.where(sb.toString());

		return apply(query.findUnique());
	}

	/**
	 * isEmpty
	 * 
	 * @param str
	 * 
	 */
	private boolean isEmpty(String str) {
		if (str == null || str.isEmpty()) {
			return true;
		}
		return false;
	}

	/**
	 * apply
	 * 
	 * @param value
	 */
	private <A> F.Option<A> apply(A value) {
		if (value != null) {
			return F.Option.Some(value);
		} else {
			return F.Option.None();
		}
	}

	@Override
	public Option<User> findById(Integer id) {
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}

	@Override
	public Option<User> save(User entry) {
		Option<User> optionUser = apply(entry);
		optionUser.get().save();
		return optionUser;
	}
}
