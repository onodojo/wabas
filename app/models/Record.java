package models;

import java.util.Date;

import javax.annotation.Nonnull;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import play.db.ebean.Model;

import com.avaje.ebean.annotation.CreatedTimestamp;

/**
 * UserBean
 *
 * @author n.ohama
 *
 */
@Entity
@Table(name="t_record")
public class Record extends Model {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	public Long id;
	@Nonnull
    public int score;
	@Nonnull
    public int point;
}
