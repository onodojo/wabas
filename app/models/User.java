package models;

import java.util.Date;

import javax.annotation.Nonnull;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import play.db.ebean.Model;

import com.avaje.ebean.annotation.CreatedTimestamp;

/**
 * UserBean
 *
 * @author n.ohama
 *
 */
@Entity
@Table(name="m_user")
public class User extends Model {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	public Long id;
	@Nonnull
	@Column(name="login_id")
	public String loginId;
	@Nonnull
	public String password;
	@Nonnull
	public String name;
	public String image;
	
	public static Finder<Long,User> find = new Finder<Long,User>(Long.class,User.class);

}
